import pickle, sys
from socket import socket
from threading import Thread

class client:                                       # Самостоятельный класс клиента
    state = True

    def __init__(self, name, addr, conn, id, server): 
        self.name = name        # Имя
        self.addr = addr        # Адрес
        self.conn = conn        # Информация о соединение с клиентом
        self.id = id            # id клиента ( не используется )
        self.server = server    # Ссылка на сервер
        conn.send(pickle.dumps({"name":server.name, "message":id, "type":"id"})) # Ответ на удачное первое соеденение!
        self.t1 = Thread(target=self.main, args=(name, conn, addr))
        self.t1.start()

    def main(self, name, conn, addr):               # Основной цикл 
        while True:
            try:
                data = conn.recv(1024)
                try:
                    obj = pickle.loads(data)
                    if self.server.debug: print(f"debug{self.name} : {obj};")
                except EOFError:
                    print("Error obj:", obj)
                except:
                    print(f"Error user {self.name} 1")
                try:
                    if obj['type'] == 'Last_request':
                        print(f"Close connection: {addr}")
                        self.state = False
                        self._close()
                        break 
                    elif obj['type'] == 'Messange':
                        obj["addr"] = self.addr
                        self.server.messages.append(obj) 						# Отправляем сообщение другим
                    else:
                        print(obj['type'], obj['type'] == 'Messange')
                except UnboundLocalError:    
                    print(data)
                except:
                    print(f"Error user {self.name} 1")
            except ConnectionResetError or ConnectionAbortedError:
                print(f"Send Error: close connection: {name} : {addr}")
                break
        if not self.state: self._close()            # Завершаем работу если соединение закрыто по какой либо причине
                
    def __str__(self):                              # Если спросят, скажи имя
        return self.name

    def send(self, data):                           # Отпрака сообщений     
        try:
            self.conn.send(pickle.dumps(data))
        except ConnectionResetError or ConnectionAbortedError:                  
            print(f"Send Error: close connection: {self.name} : {self.addr}")
            self._close()

    def _close(self):                               # Метод закрытия 
        if not self.state: return
        self.state = False
        try:
            self.send(
                {
                    "name":self.server.name,		# Имя клиента
                    "type":"Last_request",		    # Тип отправляемого сообщения, в данном случае он отправляет последний запрос к серверу
                } 								
                )
        except:
            print(f"Пользыватель {self.name} оборвал соеенение.")
        self.conn.close()
        print(self.server.clients.remove(self))

class Server:                                       # Класс сервера
    name = "Test Server"    # Имя сервера
    clients = []            # Список подключенных клиентов
    messages = []           # "Очередь" сообщений
    state = True            # Стаус сервера
    debug = False

    def __init__(self, ip, port, listen):
        self.sock = socket()
        self.sock.bind((ip, port))
        t1 = Thread(target=self.info_panel)
        t2 = Thread(target=self.demon_send)
        t1.start()
        t2.start()
        print(f"Server {self.name} started\n")
        self.Listen(listen)
    
    def Listen(self,lstn):                          # Метод принятия подключений к серверу
        self.sock.settimeout(5.0) 			        # Лимит ожидания, вызывает исключение TimeoutError
        while self.state:
            self.sock.listen(lstn)
            conn, addr = self.sock.accept()
            data = conn.recv(1024)
            obj = pickle.loads(data)
            if obj['type'] == 'First_request':       # Обрабатываем подкючение только если это ['type'] == 'First_request'
                for i in self.clients:
                    if i.name == obj["name"]:        # Если имя уже используется, то отправляем сообщение с ошибкой
                        print(f'Connect error: {obj}; \nName used.\n')
                        conn.send(pickle.dumps({"name":self.name,  "type":"Error_connect", "message":"Error 1: Name used"}))
                        conn.close()
                        break       
                else:                                # !!!!!!!!!!!!! А если вс ок, то подключем клиента
                    self.clients.append(client(obj["name"], addr, conn, len(self.clients), self))
                    print(f'Connect: {addr[0]}: {obj["name"]}')

            else:
                print(f'Connect error: {obj}')
                conn.close()   

    def info_panel(self):
        while self.state:
            data = input(":: ")
            if data == '/q':                        # Отключаем сервер
                print(f"Close server")
                for i in self.clients: i._close()   # Закрываем всех подключенных клиентов
                self.sock.close()
                sys.exit()
            elif 'send' in data:                    #
                d = data.split('send')              # Используй команду Send для отправки сообщений от имяни сервера 
                for i in self.clients:              #
                    i.send(d[1])                    #
            elif data == 'debug on':
                self.debug = True
            elif data == 'debug off':
                self.debug = False
            elif data:
                for i in self.clients:
                    print(i)

    def demon_send(self):                           # Проверяет и отправляет сообщение из очереди
        while self.state:
            if self.messages:
                messange = self.messages.pop(0)
                for i in self.clients:
                    if i.addr != messange["addr"]:
                        i.send(messange)



#----------------------------- OLD -----------------------------------
def main(conn,addr):
    while True:
        data = conn.recv(1024)
        if data == b'/q':
            print(f"Close connection: {addr}")
            break 
        elif data:
            print('Client:',data.decode('utf-8'))

def send_(con):
    while True:
        a = input()
        if a == '/q':
            print(f"Close connection")
            con.send(a.encode('utf-8'))
            con.close()  
            break
        con.send(a.encode('utf-8'))

def start_test_server():
    a = Server('', 9091, 3)

    return
    sock = socket()
    sock.bind(('', 9090))
    sock.listen(2)
    conn, addr = sock.accept()
    print(f'Connect: {addr}')
    t1 = Thread(target=main, args=(conn,addr))
    t2 = Thread(target=send_, args=(conn,))
    t1.start()
    t2.start()