from socket import socket
from threading import Thread
import pickle, sys

class Client:
	state = True													# Переменная для проверки состояния сервера
	S_object = {}													# Информация о сервере

	def __init__(self,name):
		print("Your name", name)
		self.name = name
		self.sock = socket()
		self.sock.connect(('', 9091))
		if self.__first_request():
			self.__close()
			print("Error: No first request.")
			return 
		print("\t\t Welcome to", self.S_object['name'])
		t1 = Thread(target=self.__demon_receive_messange)			# Демон для принятия сообщений
		t2 = Thread(target=self.__demon_send)						# Демон для отправки сообщений
		t1.start()
		t2.start()
	
	def __first_request(self):										# Отправляет первый запрос к серверу 
		self.sock.send(pickle.dumps(
			{
				"name":self.name,			# Имя клиента
				"type":"First_request",		# Тип отправляемого сообщения, в данном случае он отправляет первичный запрос к серверу
				"message":""				# Сообщение может быть пустым или особым ( пока не придумал для чего, может удалить? )
			} 								# Запрос может быть отклонен, например по причине занятого имени
			))
		data = self.sock.recv(1024)			
		self.S_object = pickle.loads(data)
		
		print(self.S_object)				# debug
		
		if "Error_" in self.S_object['type']:						# Обрабатываем и выводим сообщение ошибки
			#print(f"Error {self.S_object["type"].split("Error_")[1]}: {self.S_object['message']}")
			return 1
		return 0

	def __demon_send(self):											# Демон для отправки сообщений
		while self.state:
			data = input()
			if data == '/q':
				self.__close()
				sys.exit()
			elif data:

				self.send(data)

	def __demon_receive_messange(self):								# Демон для принятия сообщений
		self.sock.settimeout(5.0) 			#	Лимит ожидания, вызывает исключение TimeoutError
		while self.state:
			try:
				data = self.sock.recv(1024)
				try:
					obj = pickle.loads(data)
					if not isinstance(obj,dict):
						return
				except EOFError:
					print(data)
				if obj['type'] == 'Last_request':
					print(f"Close connection: {self.name}")
					self.__close()
					break 
				elif obj['type'] == "Messange":
					print(obj['name'],':',obj["message"])
			except ConnectionResetError or ConnectionAbortedError:
				print(f"Send Error: close connection: server")
				self.__close()
				break

	def send(self,message):											# Отправляем сообщение ( нужно сделать приватным )
			try:
				self.sock.send(pickle.dumps({
					"name":self.name,						# Имя клиента
					"type":"Messange",					# Тип отправляемого сообщения, в данном случае он отправляет последний запрос к серверу
					"message": message
				}))
			except ConnectionResetError:
				print(f"Send Error: close connection.")
				self.state = False
				self.sock.close()
			except:
				print('Error')

	def __close(self):
		self.state = False
		try:
			self.sock.send(pickle.dumps(
				{
					"name":self.name,						# Имя клиента
					"type":"Last_request",					# Тип отправляемого сообщения, в данном случае он отправляет последний запрос к серверу
				} 								
				))										# Отправляем команду на отключение
		except:
			print("Сервер не доступен.")
		self.sock.close()

	def __del__(self):												# Нужно бы написать -|-
		pass



# ----------------------------- OLD -----------------------------------

def accept_(sock):
	while True:
		try:
			data = sock.recv(1024)
			if data == b'/q':
				sock.close()
				break
			print('Server:',data.decode('utf-8'))
		except ConnectionResetError:
			sock.close()
			break

def send_(sock):
	while True:
		a = input()
		if a == '/q':			
			print(f"Close connection")
			sock.send(a.encode('utf-8'))
			sock.close()
			break	
		sock.send(a.encode('utf-8'))

def start_test_client():
	
	a = Client(input("Введите имя клиента: "))
	return
	sock = socket()
	sock.connect(('localhost', 9091))

	t1 = Thread(target=accept_, args=(sock,))
	t2 = Thread(target=send_, args=(sock,))
	t1.start()
	t2.start()
